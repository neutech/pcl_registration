cmake_minimum_required(VERSION 2.8.3)
project(pcl_registration)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  pcl_conversions
  pcl_ros
  roscpp
  sensor_msgs
  std_msgs
  cv_bridge
  image_geometry
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

find_package(PCL REQUIRED)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES ca_rangeflow
   CATKIN_DEPENDS pcl_conversions pcl_ros roscpp sensor_msgs std_msgs cv_bridge image_geometry
#  DEPENDS system_lib
)
###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
)

## Declare a cpp library
# add_library(my_pcl_tutorial
#   src/${PROJECT_NAME}/my_pcl_tutorial.cpp
# )

## Declare a cpp executable
# add_executable(my_pcl_tutorial_node src/my_pcl_tutorial_node.cpp)

## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
# add_dependencies(my_pcl_tutorial_node my_pcl_tutorial_generate_messages_cpp)

## Specify libraries to link a library or executable target against
# target_link_libraries(my_pcl_tutorial_node
#   ${catkin_LIBRARIES}
# )
link_directories(
  ${PCL_LIBRARY_DIRS}
)

add_definitions(${PCL_DEFINITIONS})


add_executable(icp src/icp.cpp)
target_link_libraries(icp ${catkin_LIBRARIES} ${PCL_LIBRARIES})

add_executable(ndt src/ndt.cpp)
target_link_libraries(ndt ${catkin_LIBRARIES} ${PCL_LIBRARIES} )

add_executable(gicp src/gicp.cpp)
target_link_libraries(gicp ${catkin_LIBRARIES} ${PCL_LIBRARIES} )
