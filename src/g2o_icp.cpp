#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
// PCL specific includes
// #include <pcl/ros/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/icp.h>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <tf/transform_broadcaster.h>
#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>
#include <pcl/registration/transformation_estimation_point_to_plane.h>

#include <pcl/registration/correspondence_rejection_var_trimmed.h>
#include <pcl/registration/correspondence_rejection_trimmed.h>
#include <pcl/registration/correspondence_rejection_surface_normal.h>
#include <pcl/registration/correspondence_rejection_one_to_one.h>
#include <pcl/registration/correspondence_rejection_distance.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>


#include <g2o/types/slam3d/types_slam3d.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/core/optimization_algorithm_gauss_newton.h>
#include <g2o/solvers/csparse/linear_solver_csparse.h>
#include <g2o/core/robust_kernel.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/optimization_algorithm_levenberg.h>


bool first_run = true;
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_new ( new pcl::PointCloud<pcl::PointXYZ> );
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_old ( new pcl::PointCloud<pcl::PointXYZ> );
Eigen::Matrix4f pose ( 4,4 );


/** Function Details
* Function Name     -- CalculateTransform
* Function Purpose  -- callback function that is called everytime we recieve a point cloud
* Function Input    -- pointCloud
* Function Output   -- Relative Transformation between two Points Cloud.
*/

void CalculateTransform ( const sensor_msgs::PointCloud2ConstPtr& input )
{

  //std::cout << "arrive here!" << std::endl;
  if ( first_run )
  {

    if ( ( input->height * input->width ) > 0 )
    {
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg ( *input,cloud );

      std::vector<int> temp;
      pcl::removeNaNFromPointCloud ( cloud, cloud, temp );

      // Fill in the CloudIn data
      cloud_new->width    = cloud.width;
      cloud_new->height   = cloud.height;
      cloud_new->is_dense = false;
      cloud_new->points.resize ( cloud_new->width * cloud_new->height );
      for ( size_t i = 0; i < cloud_new->points.size (); ++i )
      {
        cloud_new->points[i].x = cloud.points[i].x;
        cloud_new->points[i].y = cloud.points[i].y;
        cloud_new->points[i].z = cloud.points[i].z;
      }

      *cloud_old = *cloud_new;

      first_run = false;
    }
  }

  if ( first_run == false )
  {
    ros::WallTime startTime = ros::WallTime::now();

    if ( ( input->height * input->width ) > 0 )
    {
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg ( *input,cloud );

      std::vector<int> temp;
      pcl::removeNaNFromPointCloud ( cloud, cloud, temp );

      // Fill in the CloudIn data
      cloud_new->width    = cloud.width;
      cloud_new->height   = cloud.height;
      cloud_new->is_dense = false;
      cloud_new->points.resize ( cloud_new->width * cloud_new->height );
      for ( size_t i = 0; i < cloud_new->points.size (); ++i )
      {
        cloud_new->points[i].x = cloud.points[i].x;
        cloud_new->points[i].y = cloud.points[i].y;
        cloud_new->points[i].z = cloud.points[i].z;
      }
    }
    pcl::PointCloud<pcl::PointNormal>::Ptr src ( new pcl::PointCloud<pcl::PointNormal> );
    pcl::PointCloud<pcl::PointNormal>::Ptr tgt ( new pcl::PointCloud<pcl::PointNormal> );

    pcl::copyPointCloud ( *cloud_new, *src );
    pcl::copyPointCloud ( *cloud_old, *tgt );

    pcl::NormalEstimation<pcl::PointNormal, pcl::PointNormal> norm_est;
    norm_est.setSearchMethod ( pcl::search::KdTree<pcl::PointNormal>::Ptr ( new pcl::search::KdTree<pcl::PointNormal> ) );
    norm_est.setKSearch ( 5 );
    norm_est.setInputCloud ( tgt );
    norm_est.compute ( *tgt );


    pcl::IterativeClosestPoint<pcl::PointNormal, pcl::PointNormal> icp;
    typedef pcl::registration::TransformationEstimationPointToPlaneLLS<pcl::PointNormal, pcl::PointNormal> PointToPlane;
    boost::shared_ptr<PointToPlane> point_to_plane ( new PointToPlane );
    icp.setTransformationEstimation ( point_to_plane );

//     boost::shared_ptr<pcl::registration::CorrespondenceRejectorDistance> rejector_distance ( new pcl::registration::CorrespondenceRejectorDistance );
//     rejector_distance->setInputSource<pcl::PointNormal> ( src );
//     rejector_distance->setInputTarget<pcl::PointNormal> ( tgt );
//     rejector_distance->setMaximumDistance ( 0.05 );
// 
//     icp.addCorrespondenceRejector ( rejector_distance );


    icp.setInputSource ( src );
    icp.setInputTarget ( tgt );

    // Set the max correspondence distance to 5cm (e.g., correspondences with higher distances will be ignored)
    icp.setMaxCorrespondenceDistance ( 0.05 );
    // Set the maximum number of iterations (criterion 1)
    icp.setMaximumIterations ( 30 );
    // Set the transformation epsilon (criterion 2)
    icp.setTransformationEpsilon ( 1e-8 );
    // Set the euclidean distance difference epsilon (criterion 3)
    icp.setEuclideanFitnessEpsilon ( 0.001 );


    pcl::PointCloud<pcl::PointNormal> Final;
    icp.align ( Final );


    std::cout << "has converged:" << icp.hasConverged() << " score: " <<
              icp.getFitnessScore() << std::endl;

    std::cout << icp.getFinalTransformation() << std::endl;

    pose = pose * icp.getFinalTransformation();

    std::cout << "x = " << pose ( 0,3 ) << "  y = " << pose ( 1,3 ) << "   z= " << pose ( 2,3 ) << std::endl;

    static tf::TransformBroadcaster br;
    tf::Matrix3x3 rot_mat (
      pose ( 0,0 ), pose ( 0,1 ), pose ( 0,2 ),
      pose ( 1,0 ), pose ( 1,1 ), pose ( 1,2 ),
      pose ( 2,0 ), pose ( 2,1 ), pose ( 2,2 ) );
    tf::Vector3 t ( pose ( 0,3 ), pose ( 1,3 ), pose ( 2,3 ) );
    tf::Transform transform ( rot_mat, t );

    br.sendTransform ( tf::StampedTransform ( transform, input->header.stamp, "/world", "/camera_rgb_optical_frame" ) );

    std::cout << "Total ICP estimation took: " << ( ros::WallTime::now() - startTime ).toSec() << std::endl;
    *cloud_old = *cloud_new;
  }


};

int main ( int argc, char **argv )
{

  ros::init ( argc, argv, "pcl_icp" );
  pose = Eigen::Matrix4f::Identity();

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe ( "input", 1, CalculateTransform );

  ros::spin();

  return 0;
}

