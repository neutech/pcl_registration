#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
// PCL specific includes
// #include <pcl/ros/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/icp.h>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <tf/transform_broadcaster.h>
#include <pcl/filters/filter.h>
#include <pcl/registration/ndt.h>
#include <pcl/filters/approximate_voxel_grid.h>


ros::Publisher vis_pub; //ros publisher to publish markers
ros::Publisher cloud_pub;
ros::Publisher prev_cloud_pub;

bool first_run = true;
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_new ( new pcl::PointCloud<pcl::PointXYZ> );
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_old ( new pcl::PointCloud<pcl::PointXYZ> );
Eigen::Matrix4f pose ( 4,4 );
Eigen::Matrix4f preTrans;

visualization_msgs::Marker marker;
geometry_msgs::Point p;


/** Function Details
* Function Name     -- CalculateTransform
* Function Purpose  -- callback function that is called everytime we recieve a point cloud
* Function Input    -- pointCloud
* Function Output   -- Relative Transformation between two Points Cloud.
*/

void CalculateTransform ( const sensor_msgs::PointCloud2ConstPtr& input )
{

  if ( first_run )
  {

    if ( ( input->height * input->width ) > 0 )
    {
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg ( *input,cloud );

      std::vector<int> temp;
      pcl::removeNaNFromPointCloud ( cloud, cloud, temp );

      // Fill in the CloudIn data
      cloud_new->width    = cloud.width;
      cloud_new->height   = cloud.height;
      cloud_new->is_dense = false;
      cloud_new->points.resize ( cloud_new->width * cloud_new->height );
      for ( size_t i = 0; i < cloud_new->points.size (); ++i )
      {
        cloud_new->points[i].x = cloud.points[i].x;
        cloud_new->points[i].y = cloud.points[i].y;
        cloud_new->points[i].z = cloud.points[i].z;
      }

      *cloud_old = *cloud_new;

      first_run = false;
    }
  }

  if ( first_run == false )
  {
    ros::WallTime startTime = ros::WallTime::now();

    if ( ( input->height * input->width ) > 0 )
    {
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg ( *input,cloud );

      std::vector<int> temp;
      pcl::removeNaNFromPointCloud ( cloud, cloud, temp );

      // Fill in the CloudIn data
      cloud_new->width    = cloud.width;
      cloud_new->height   = cloud.height;
      cloud_new->is_dense = false;
      cloud_new->points.resize ( cloud_new->width * cloud_new->height );
      for ( size_t i = 0; i < cloud_new->points.size (); ++i )
      {
        cloud_new->points[i].x = cloud.points[i].x;
        cloud_new->points[i].y = cloud.points[i].y;
        cloud_new->points[i].z = cloud.points[i].z;
      }
    }


    // Initializing Normal Distributions Transform (NDT).
    pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ> ndt;

    // Setting scale dependent NDT parameters
    // Setting minimum transformation difference for termination condition.
    ndt.setTransformationEpsilon ( 0.001 );
    // Setting maximum step size for More-Thuente line search.
    ndt.setStepSize ( 0.01 );
    //Setting Resolution of NDT grid structure (VoxelGridCovariance).
    ndt.setResolution ( 0.5 );

    // Setting max number of registration iterations.
    ndt.setMaximumIterations ( 30 );

    // Setting point cloud to be aligned.
    ndt.setInputSource ( cloud_new );
    // Setting point cloud to be aligned to.
    ndt.setInputTarget ( cloud_old );

    // Set initial alignment estimate found using robot odometry.
    Eigen::AngleAxisf init_rotation ( 0, Eigen::Vector3f::UnitZ () );
    Eigen::Translation3f init_translation ( 0, 0.0, 0 );
// 		Eigen::Matrix4f init_guess = (init_translation * init_rotation).matrix ();
    Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();

    // Calculating required rigid transform to align the input cloud to the target cloud.
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud ( new pcl::PointCloud<pcl::PointXYZ> );
    ndt.align ( *output_cloud, init_guess );


    pcl::transformPointCloud ( *cloud_new, *output_cloud, ndt.getFinalTransformation () );
    sensor_msgs::PointCloud2 aligned_cloud;
    pcl::toROSMsg ( *output_cloud, aligned_cloud );
    aligned_cloud.header.frame_id = input->header.frame_id;
    cloud_pub.publish ( aligned_cloud );


    sensor_msgs::PointCloud2 prev_cloud;
    pcl::toROSMsg ( *cloud_old, prev_cloud );
    prev_cloud.header.frame_id = input->header.frame_id;
    prev_cloud_pub.publish ( prev_cloud );

    std::cout << "Normal Distributions Transform has converged:" << ndt.hasConverged ()
              << " score: " << ndt.getFitnessScore () << std::endl;
    std::cout << "Final Transform is: " << ndt.getFinalTransformation() << std::endl;

    pose = pose * ndt.getFinalTransformation();
    std::cout << "x = " << pose ( 0,3 ) << "  y = " << pose ( 1,3 ) << "   z= " << pose ( 2,3 ) << std::endl;



    static tf::TransformBroadcaster br;
    tf::Matrix3x3 rot_mat (
      pose ( 0,0 ), pose ( 0,1 ), pose ( 0,2 ),
      pose ( 1,0 ), pose ( 1,1 ), pose ( 1,2 ),
      pose ( 2,0 ), pose ( 2,1 ), pose ( 2,2 ) );
    tf::Vector3 t ( pose ( 0,3 ), pose ( 1,3 ), pose ( 2,3 ) );
    tf::Transform transform ( rot_mat, t );
    //transform.setOrigin(tf::Vector3(0, 0, 0.0));

    br.sendTransform ( tf::StampedTransform ( transform, input->header.stamp, "/world", "/camera_rgb_optical_frame" ) );

    std::cout << "Total NDT estimation took: " << ( ros::WallTime::now() - startTime ).toSec() << std::endl;
    *cloud_old = *cloud_new;
  }
};



int main ( int argc, char **argv )
{

  pose = Eigen::Matrix4f::Identity();
  preTrans = Eigen::Matrix4f::Identity();

  ros::init ( argc, argv, "depthOdometry" );

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe ( "input", 1, CalculateTransform );
  cloud_pub = n.advertise<sensor_msgs::PointCloud2> ( "/aligned_cloud",5 );
  prev_cloud_pub = n.advertise<sensor_msgs::PointCloud2> ( "/prev_cloud",5 );

  ros::spin();

  return 0;
}
