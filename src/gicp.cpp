#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
// PCL specific includes
// #include <pcl/ros/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/registration/gicp.h>

#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <tf/transform_broadcaster.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>


using namespace std;

ros::Publisher cloud_pub;
ros::Publisher prev_cloud_pub;


bool first_run = true;
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_new ( new pcl::PointCloud<pcl::PointXYZ> );
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_old ( new pcl::PointCloud<pcl::PointXYZ> );
Eigen::Matrix4f pose ( 4,4 );
FILE* fileVar;


/** Function Details
* Function Name     -- CalculateTransform
* Function Purpose  -- callback function that is called everytime we recieve a point cloud
* Function Input    -- pointCloud
* Function Output   -- Relative Transformation between two Points Cloud.
*/

void CalculateTransform ( const sensor_msgs::PointCloud2ConstPtr& input )
{


  //std::cout << "arrive here!" << std::endl;
  if ( first_run )
  {

    if ( ( input->height * input->width ) > 0 )
    {
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg ( *input,cloud );

      std::vector<int> temp;
      pcl::removeNaNFromPointCloud ( cloud, cloud, temp );

      // Fill in the CloudIn data
      cloud_new->width    = cloud.width;
      cloud_new->height   = cloud.height;
      cloud_new->is_dense = false;
      cloud_new->points.resize ( cloud_new->width * cloud_new->height );
      for ( size_t i = 0; i < cloud_new->points.size (); ++i )
      {
        cloud_new->points[i].x = cloud.points[i].x;
        cloud_new->points[i].y = cloud.points[i].y;
        cloud_new->points[i].z = cloud.points[i].z;
      }

      *cloud_old = *cloud_new;

      first_run = false;
    }
  }

  if ( first_run == false )
  {
    ros::WallTime start = ros::WallTime::now();

    if ( ( input->height * input->width ) > 0 )
    {
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg ( *input,cloud );

      std::vector<int> temp;
      pcl::removeNaNFromPointCloud ( cloud, cloud, temp );

      // Fill in the CloudIn data
      cloud_new->width    = cloud.width;
      cloud_new->height   = cloud.height;
      cloud_new->is_dense = false;
      cloud_new->points.resize ( cloud_new->width * cloud_new->height );
      for ( size_t i = 0; i < cloud_new->points.size (); ++i )
      {
        cloud_new->points[i].x = cloud.points[i].x;
        cloud_new->points[i].y = cloud.points[i].y;
        cloud_new->points[i].z = cloud.points[i].z;
      }
    }
    pcl::PointCloud<pcl::PointNormal>::Ptr src ( new pcl::PointCloud<pcl::PointNormal> );
    pcl::PointCloud<pcl::PointNormal>::Ptr tgt ( new pcl::PointCloud<pcl::PointNormal> );

    pcl::copyPointCloud ( *cloud_new, *src );
    pcl::copyPointCloud ( *cloud_old, *tgt );

    pcl::NormalEstimation<pcl::PointNormal, pcl::PointNormal> norm_est;
    norm_est.setSearchMethod ( pcl::search::KdTree<pcl::PointNormal>::Ptr ( new pcl::search::KdTree<pcl::PointNormal> ) );
    norm_est.setKSearch ( 5 );
    norm_est.setInputCloud ( tgt );
    norm_est.compute ( *tgt );

    //GICP
    pcl::GeneralizedIterativeClosestPoint<pcl::PointNormal, pcl::PointNormal> gicp;
    gicp.setInputSource ( src );
    gicp.setInputTarget ( tgt );


    pcl::PointCloud<pcl::PointNormal> Final1;
    gicp.align ( Final1 );
    std::cout << "GICP has converged:" << gicp.hasConverged() << " score: " << gicp.getFitnessScore() << std::endl;
    pose = pose * gicp.getFinalTransformation();


    sensor_msgs::PointCloud2 aligned_cloud;
    pcl::toROSMsg ( Final1, aligned_cloud );
    aligned_cloud.header.frame_id = input->header.frame_id;
    cloud_pub.publish ( aligned_cloud );


    sensor_msgs::PointCloud2 prev_cloud;
    pcl::toROSMsg ( *cloud_old, prev_cloud );
    prev_cloud.header.frame_id = input->header.frame_id;
    prev_cloud_pub.publish ( prev_cloud );


    std::cout << "x = " << pose ( 0,3 ) << "  y = " << pose ( 1,3 ) << "   z= " << pose ( 2,3 ) << std::endl;

    static tf::TransformBroadcaster br;
    tf::Matrix3x3 rot_mat (
      pose ( 0,0 ), pose ( 0,1 ), pose ( 0,2 ),
      pose ( 1,0 ), pose ( 1,1 ), pose ( 1,2 ),
      pose ( 2,0 ), pose ( 2,1 ), pose ( 2,2 ) );
    tf::Vector3 t ( pose ( 0,3 ), pose ( 1,3 ), pose ( 2,3 ) );
    tf::Transform transform ( rot_mat, t );

    br.sendTransform ( tf::StampedTransform ( transform, input->header.stamp, "/world", "/camera_rgb_optical_frame" ) );

    Eigen::Matrix3d R;
    for ( int row = 0; row < 3; row ++ )
    {
      for ( int col = 0; col < 3; col ++ )
      {
        R ( row, col ) = pose ( row, col );
      }
    }
    std::vector<float> euler ( 3 );
    euler[0] = atan ( R ( 1, 2 ) / R ( 2, 2 ) );
    euler[1] = asin ( -R ( 0, 2 ) );
    euler[2] = atan ( R ( 0, 1 ) / R ( 0, 0 ) );

    Eigen::Quaterniond quat ( R );

//     fprintf ( fileVar, "%f %f %f %f %f %f %f %f %f\n", input->header.stamp.toSec(),
//               pose ( 0,3 ),
//               pose ( 1,3 ),
//               pose ( 2,3 ),
//               quat.x(),
//               quat.y(),
//               quat.z(),
//               quat.w(),
//               t1.elapsed() );


    std::cout << "Total GICP estimation took: " << ( ros::WallTime::now() - start ).toSec() << endl <<endl;
    *cloud_old = *cloud_new;
  }


};

int main ( int argc, char **argv )
{

  ros::init ( argc, argv, "pcl_icp" );
  pose = Eigen::Matrix4f::Identity();
  fileVar = fopen ( "/home/fang/fuerte_workspace/sandbox/gicp.txt", "w" );

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe ( "input", 1, CalculateTransform );
  cloud_pub = n.advertise<sensor_msgs::PointCloud2> ( "/aligned_cloud",5 );
  prev_cloud_pub = n.advertise<sensor_msgs::PointCloud2> ( "/prev_cloud",5 );


  ros::spin();

  return 0;
}





